metadata:
  name: apertis-update-manager-ota-auto
  format: "Apertis Test Definition 1.0"
  image-types:
    minimal: [ armhf ]
  image-deployment:
    - OSTree
  type: functional
  exec-type: manual
  priority: medium
  maintainer: "Apertis Project"
  description: "Test the apertis-update-manager automatic updates over the air."

  resources:
    - "The DUT u-boot environment must be clean: in u-boot, run: `env default -a` followed by `saveenv`"
    - "A PC must be connected to DUT serial port"
    - "The DUT must be connected to network"

  expected:
    - "The OTA update was properly applied"

run:
  steps:
    - "Check the initial deployment"
    - $ sudo ostree admin status
    - "Prepare the copy of commit and deploy to allow the upgrade to the same version"
    - "Command below shows you an initial commit ID, for instance"
    - |
        $ export BOOTID=$(sudo ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p'); echo $BOOTID
    - "Get the Collection ID and ref"
    -  $ export CID=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 1 -d ' '); echo COLLECTION_ID=$CID
    -  $ export REF=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 2 -d ' '); echo REF=$REF
    - "Create the commit with changed timestamp to allow upgrade with recent update file"
    - |
        $ export NEWID=$(sudo ostree commit --orphan --tree=ref=$BOOTID --add-metadata-string=ostree.collection-binding=$CID --bind-ref=$REF --timestamp="1 year ago"); echo "New commit: $NEWID"
    - "Deploy the prepared commit"
    - $ sudo ostree admin upgrade --allow-downgrade --deploy-only --override-commit=$NEWID --reboot
    - "Wait until the system is booted again and check the deployment"
    - $ sudo ostree admin status
    - "The booted commit (started with '*') must have ID which we prepare and the initial commit ID should be marked as '(rollback)'"
    - "Remove the initial deployment"
    - $ sudo ostree admin undeploy 1
    - "Check the current deployment"
    - $ sudo ostree admin status
    - "Enable automatic OTA updates in configuration file '/etc/apertis-update-manager.ini'"
    - $ sudo sed -i -e "s/^UpdatesFromNetwork=.*/UpdatesFromNetwork=true/" /etc/apertis-update-manager.ini
    - "Configuration file '/etc/apertis-update-manager.ini' must have property 'UpdatesFromNetwork' set to true"
    - $ sudo grep UpdatesFromNetwork /etc/apertis-update-manager.ini
    - "Remove blacklist file if it exists"
    - $ sudo rm -f /var/aum_blacklist.conf
    - "Reboot the system"
    - $ sudo reboot
    - "Wait while the system booted successfully. Need to wait a short time (30-120 sec) depending on your network. The update must be downloaded and the system should be rebooted second time automatically.
      The current status of upgrade could be checked with journalctl log -- it should not contain error messages about the upgrade. The listing below is just for example:"
    - $ sudo journalctl -ef --unit apertis-update-manager
    - |
        >Jun 01 11:54:26 apertis apertis-update-[355]: Ostree upgrade progress: 1 metadata, 0 content objects fetched
        Jun 01 11:54:26 apertis apertis-update-[355]: Cannot check the ID in black list: No such file or directory
        Jun 01 11:57:10 apertis apertis-update-managerd[355]: libostree pull from 'origin' for apertis/v2019pre/amd64-uefi/minimal complete
                                                              security: GPG: disabled http: TLS
                                                              non-delta: meta: 23 content: 39
                                                              transfer: secs: 163 size: 32.2 MB
        Jun 01 11:57:10 apertis apertis-update-[355]: New upgrade downloaded! Deploying..
        Jun 01 11:57:10 apertis apertis-update-managerd[355]: Copying /etc changes: 13 modified, 0 removed, 1 added
        Jun 01 11:57:10 apertis apertis-update-[355]: Couldn't unlink ostree-0-1.conf
        Jun 01 11:57:11 apertis apertis-update-managerd[355]: Transaction complete; bootconfig swap: yes; deployment count change: 1
        Jun 01 11:57:11 apertis apertis-update-[355]: Ostree upgrade ready, system should be rebooted
        Jun 01 11:57:11 apertis apertis-update-[355]: Rebooting to apply pending update
    - "Check the current deployment has been updated and that the rollback entry points to the prepared deployment"
    - $ sudo ostree admin status
    - "Check the journal log should mention that the system check the update availability and report it is already up to date"
    - $ sudo journalctl -ef --unit apertis-update-manager
    - |
        >Jun 01 22:35:32 apertis apertis-update-[365]: Ostree upgrade poll starting
        Jun 01 22:35:34 apertis apertis-update-[365]: Ostree upgrade progress: 1 metadata, 0 content objects fetched
        Jun 01 22:35:34 apertis apertis-update-[365]: Ostree already up to date
